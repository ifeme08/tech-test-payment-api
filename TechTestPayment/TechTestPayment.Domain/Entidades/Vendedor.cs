﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTestPayment.Domain.ObjetosDominio;

namespace TechTestPayment.Domain.Entidades
{
    public class Vendedor : Entity
    {
        public Vendedor(string cPF, string nome, string email, string telefone)
        {
            CPF = cPF;
            Nome = nome;
            Email = email;
            Telefone = telefone;
            _vendas = new List<Venda>();
        }

        public string CPF { get; private set; }
        public string Nome { get; private set; }
        public string Email { get; private set; }
        public string Telefone { get; private set; }
        private List<Venda> _vendas { get; set; }
        public IReadOnlyCollection<Venda> Vendas => _vendas;
        public void AlterarNome(string nome)
        {
            if(Nome == nome)
            {
                _notificador.Handle(new Notificacao("Novo nome não pode ser igual ao ultimo!"));
                return;
            }
            Nome = nome;
        }

        public void AlterarEmail(string email)
        {
            if (Email == email)
            {
                _notificador.Handle(new Notificacao("Novo email não pode ser igual ao ultimo!"));
                return;
            }
            Email = email;
        }
         
        public void AlterarTelefone(string telefone)
        {
            if (Telefone == telefone)
            {
                _notificador.Handle(new Notificacao("Novo telefone não pode ser igual ao ultimo!"));
                return;
            }
            Telefone = telefone;
        }
    }
}
