﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTestPayment.Domain.ObjetosDominio;

namespace TechTestPayment.Domain.Entidades
{
    public class Item : Entity
    {
        public Item(
            string nome, 
            decimal valorUnitario, 
            int quantidade
            )
        {
            Nome = nome;
            ValorUnitario = valorUnitario;
            Quantidade = quantidade;
            AtualizarPreco();
        }

        public string Nome { get; private set; }
        public decimal Preco { get; private set; }
        public decimal ValorUnitario { get; private set; }
        public int Quantidade { get; private set; }
        public Guid VendaId { get; private set; }
        public Venda Venda { get; set; }

        public void AdicionarUnidades(int quantidade)
        {
            if(quantidade < 0 || quantidade == 0)
            {
                _notificador.Handle(new Notificacao("Não é possível inserir valores menores que 0"));
            }
            Quantidade += quantidade;
            AtualizarPreco();
        }
        public void RemoverUnidades(int quantidade)
        {
            if(quantidade > Quantidade || quantidade == Quantidade)
            {
                _notificador.Handle(new Notificacao("Quantidade não pode ser menor ou igual a zero!"));
                return;
            }
            Quantidade -= quantidade;
            AtualizarPreco();
        }

        private void AtualizarPreco()
        {
            if((Quantidade * ValorUnitario) <= 0)
            {
                _notificador.Handle(new Notificacao("Preço não pode ser menor ou igual a zero!"));
                return;
            }
           Preco = Quantidade * ValorUnitario;
        }
    }
}
