﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TechTestPayment.Domain.Entidades
{
    public enum Status
    {
        AguardandoPagamento,
        PagamentoAprovado,
        Cancelada,
        EnviadoParaTransportadora,
        Entregue,
    }
}
