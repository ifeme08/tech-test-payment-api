﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTestPayment.Domain.ObjetosDominio;

namespace TechTestPayment.Domain.Entidades
{
    public class Venda : Entity
    {
        public Venda()
        {
            Data = DateTime.Now;
            Status = Status.AguardandoPagamento;
            _itens = new List<Item>();
        }
        public DateTime Data { get; private set; }
        public Status Status { get; private set; }
        public Guid VendedorId { get; private set; }
        public virtual Vendedor Vendedor { get; private set; }
        private readonly List<Item> _itens;
        public IReadOnlyCollection<Item> Itens => _itens;

        public bool AlterarStatus(int numero)
        {
            bool result;
            switch(numero)
            {
                case 1:
                    if(Status == Status.AguardandoPagamento)
                    {
                        Status = Status.PagamentoAprovado;
                        result =  true;
                        break;
                    }
                    result = false;
                    break;
                case 2:
                    if(Status == Status.AguardandoPagamento)
                    {
                        Status = Status.Cancelada;
                        result = true;
                        break;
                    }
                    if(Status == Status.PagamentoAprovado)
                    {
                        Status = Status.Cancelada;
                        result = true;
                        break;
                    }
                    result = false;
                    break;
                case 3:
                    if(Status == Status.PagamentoAprovado)
                    {
                        Status = Status.EnviadoParaTransportadora;
                        result = true;
                        break;
                    }
                    result = false;
                    break;
                case 4:
                    if(Status == Status.EnviadoParaTransportadora)
                    {
                        Status = Status.Entregue;
                        result = true;
                        break;
                    }
                    result = false;
                    break;
                default:
                    result = false;
                    break;
            }

            if(!result)
                _notificador.Handle(new Notificacao("Não foi possível alterar o Status"));

            return result;
        }

        public void AdicionarItem(Item item)
        {
            if(ItemExistente(item))
            {
                var itemExistente = _itens.FirstOrDefault(x => x.Id == item.Id);
                itemExistente.AdicionarUnidades(item.Quantidade);
                item = itemExistente;
                _itens.Remove(itemExistente);
            }

            _itens.Add(item);

        }

        public void RemoverItem(Item item)
        {
            ValidarItemInexistente(item);

            _itens.Remove(item);
        }

        private bool ItemExistente(Item item)
        {
            return _itens.Any(x => x.Id == item.Id);
        }

        private void ValidarItemInexistente(Item item)
        {
            if (!ItemExistente(item)) _notificador.Handle(new Notificacao("Este item não pertence a venda!"));
        }
    }
}
