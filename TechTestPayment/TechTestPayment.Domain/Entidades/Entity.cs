﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTestPayment.Domain.ObjetosDominio;

namespace TechTestPayment.Domain.Entidades
{
    public abstract class Entity
    {
        public Guid Id { get; private set; }
        public Notificador _notificador;
        public Entity()
        {
            Id = Guid.NewGuid();
            _notificador = new Notificador();
        }
    }
}
