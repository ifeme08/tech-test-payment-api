﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTestPayment.Application.Validators;
using TechTestPayment.Data.Repositorios;
using TechTestPayment.Domain.Entidades;
using TechTestPayment.Domain.ObjetosDominio;

namespace TechTestPayment.Application
{
    public class VendaService : BaseService, IVendaService
    {
        private readonly IVendaRepository _repositorio;
        private VendedorValidator _vendedorValidator;
        private ItemValidator _itemValidator;
        public VendaService(
            INotificador notificador,
            VendedorValidator vendedorValidator,
            ItemValidator itemValidator,
            IVendaRepository repositorio
            ) : base(notificador)
        {
            _vendedorValidator = vendedorValidator;
            _itemValidator = itemValidator;
            _repositorio = repositorio;
        }

        public async Task<bool> Adicionar(Venda venda)
        {
            if (!ExecutarValidacao(_vendedorValidator, venda.Vendedor))
                return false;

            foreach(var item in venda.Itens)
            {
                if(item == null || !ExecutarValidacao(_itemValidator, item))
                {
                    Notificar($"Item {item.Nome} inválido. Tente Novamente!");
                    return false;
                }

            }

            return await _repositorio.Adicionar(venda);
        }

        public async Task<bool> Atualizar(Guid id, int status)
        {
            if (id == Guid.Empty)
            {
                Notificar("Identificador inválido");
                return false;
            }

            return await _repositorio.Atualizar(id, status);

        }
    }
}
