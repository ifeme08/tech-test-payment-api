﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTestPayment.Domain.Entidades;

namespace TechTestPayment.Application.Validators
{
    public class ItemValidator : AbstractValidator<Item>
    {
        public ItemValidator()
        {
            RuleFor(x => x.Nome)
                .NotEmpty().WithMessage("O campo {PropertyName} precisa ser fornecido")
                .Length(5, 50).WithMessage("O campo {PropertyName} precisa ter entre {MinLength} e {MaxLength} caracteres");
            RuleFor(x => x.Quantidade)
                .NotEmpty().WithMessage("O campo {PropertyName} precisa ser fornecido")
                .NotNull().WithMessage("O campo {PropertyName} precisa ser fornecido")
                .GreaterThan(0).WithMessage("Não é possível cadastrar um item com quantidade igual a 0");
            RuleFor(x => x.ValorUnitario)
                .NotEmpty().WithMessage("O campo {PropertyName} precisa ser fornecido")
                .NotNull().WithMessage("O campo {PropertyName} precisa ser fornecido")
                .GreaterThan(0).WithMessage("Não é possível cadastrar um item com valor igual a 0");
        }
    }
}
