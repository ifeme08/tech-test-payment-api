﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTestPayment.Domain.Entidades;

namespace TechTestPayment.Tests.Domain
{
    public class VendedorTests
    {
        [Fact(DisplayName = "Vendedor Altera Nome Com Falha")]
        [Trait("Categoria", "Vendas - Vendedor")]
        public void NovoVendedor_AlterarNome_DeveDispararNotificacao()
        {
            // Arrange
            var vendedor = new Vendedor("02567732543", "Icaro Felix", "ifeme08@gmail.com", "71992368543");
            var notificador = vendedor._notificador;
            var mensagem = "Novo nome não pode ser igual ao ultimo!";
            // Act
            vendedor.AlterarNome("Icaro Felix");
            var notificacao = notificador.ObterNotificacoes().FirstOrDefault();

            // Assert
            Assert.True(notificador.TemNotificacao());
            Assert.Equal(notificacao?.Mensagem, mensagem);
        }

        [Fact(DisplayName = "Vendedor Altera Email Com Falha")]
        [Trait("Categoria", "Vendas - Vendedor")]
        public void NovoVendedor_AlterarEmail_DeveDispararNotificacao()
        {
            // Arrange
            var vendedor = new Vendedor("02567732543", "Icaro Felix", "ifeme08@gmail.com", "71992368543");
            var notificador = vendedor._notificador;
            var mensagem = "Novo email não pode ser igual ao ultimo!";
            // Act
            vendedor.AlterarEmail("ifeme08@gmail.com");
            var notificacao = notificador.ObterNotificacoes().FirstOrDefault();

            // Assert
            Assert.True(notificador.TemNotificacao());
            Assert.Equal(notificacao?.Mensagem, mensagem);
        }

        [Fact(DisplayName = "Vendedor Altera Telefone Com Falha")]
        [Trait("Categoria", "Vendas - Vendedor")]
        public void NovoVendedor_AlterarTelefone_DeveDispararNotificacao()
        {
            // Arrange
            var vendedor = new Vendedor("02567732543", "Icaro Felix", "ifeme08@gmail.com", "71992368543");
            var notificador = vendedor._notificador;
            var mensagem = "Novo telefone não pode ser igual ao ultimo!";
            // Act
            vendedor.AlterarTelefone("71992368543");
            var notificacao = notificador.ObterNotificacoes().FirstOrDefault();

            // Assert
            Assert.True(notificador.TemNotificacao());
            Assert.Equal(notificacao?.Mensagem, mensagem);
        }
    }
}
