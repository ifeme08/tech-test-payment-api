﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTestPayment.Domain.Entidades;

namespace TechTestPayment.Tests.Domain
{
    public class ItensTests
    {
        [Fact(DisplayName = "Novo Item com unidades abaixo do permitido")]
        [Trait("Categoria", "Vendas - Item")]
        public void NovoItem_UnidadesIgualAZero_DeveRetornarNotificacao()
        {
            // arrange
            var item = new Item("Budega", 35.5m, 10);
            var notificador = item._notificador;

            // act

            item.RemoverUnidades(10);

            // assert

            Assert.True(notificador.TemNotificacao());
            Assert.False(item.Quantidade == 0);
        }

        [Fact(DisplayName = "Novo Item com unidades abaixo do permitido")]
        [Trait("Categoria", "Vendas - Item")]
        public void NovoItem_UnidadesAbaixoDeZero_DeveRetornarNotificacao()
        {
            // arrange
            var item = new Item("Budega", 35.5m, 10);
            var notificador = item._notificador;

            // act

            item.RemoverUnidades(11);

            // assert

            Assert.True(notificador.TemNotificacao());
            Assert.False(item.Quantidade == 0 || item.Quantidade < 0);
        }

        [Fact(DisplayName = "Novo Item Atualiza Preço")]
        [Trait("Categoria", "Vendas - Item")]
        public void NovoItem_AdicionarItens_DeveAtualizarPreco_Corretamente()
        {
            // arrange
            var item1 = new Item("Budega", 35.5m, 10);
            var item2 = new Item("Bagulho", 20.0m, 20);
            // act

            item1.AdicionarUnidades(10);
            item2.AdicionarUnidades(20);

            // assert

            Assert.True(item1.Preco == 710.0m);
            Assert.True(item2.Preco == 800.0m);
        }
    }
}
