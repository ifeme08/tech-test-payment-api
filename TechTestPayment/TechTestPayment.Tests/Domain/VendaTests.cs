﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTestPayment.Domain.Entidades;

namespace TechTestPayment.Tests.Domain
{
    public class VendaTests
    {
        [Fact(DisplayName = "Alterar Venda Verdadeiro")]
        [Trait("Categoria", "Vendas")]
        public void AlterarVenda_AlterarStatus_DeveRetornarTrue()
        {
            //Arrange
            var venda = new Venda();

            //Act
            var result = venda.AlterarStatus(1);

            //Assert
            Assert.True(result);
            Assert.True(venda.Status == Status.PagamentoAprovado);
        }

        [Fact(DisplayName = "Alterar Venda Falso")]
        [Trait("Categoria", "Vendas")]
        public void AlterarVenda_AlterarStatus_DeveRetornarFalse()
        {
            //Arrange
            var venda = new Venda();

            //Act
            var result = venda.AlterarStatus(4);

            //Assert
            Assert.False(result);
            Assert.True(venda.Status == Status.AguardandoPagamento);
        }

        [Fact(DisplayName = "Alterar Venda Cancelada")]
        [Trait("Categoria", "Vendas")]
        public void AlterarVenda_AlterarStatus_DeveRetornarStatusCancelado()
        {
            //Arrange
            var venda1 = new Venda();
            var venda2 = new Venda();

            venda2.AlterarStatus(1);
            //Act
            var result1 = venda1.AlterarStatus(2);
            var result2 = venda2.AlterarStatus(2);

            //Assert
            Assert.True(result1);
            Assert.True(venda1.Status == Status.Cancelada);
            Assert.True(result2);
            Assert.True(venda2.Status == Status.Cancelada);
        }
    }
}
