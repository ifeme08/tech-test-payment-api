﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TechTestPayment.Application;
using TechTestPayment.Data.Repositorios;
using TechTestPayment.Domain.Entidades;
using TechTestPayment.Domain.ObjetosDominio;

namespace TechTestPayment.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class VendasController : MainController
    {
        private readonly IVendaService _service;
        private readonly IVendaRepository _repository;
        public VendasController(INotificador notificador) : base(notificador)
        {
           
        }

        [HttpGet("buscar-venda/{id:guid}")]
        public async Task<ActionResult<Venda>> BuscarVenda([FromRoute] Guid id)
        {
            try
            {
                if (id == Guid.Empty) return BadRequest("Identificador inválido");

                var result = await _repository.BuscarVenda(id);

                if (result == null) return NotFound("Nenhum objeto encontrado");

                return Ok(result);

            }
            catch (Exception ex)
            {
                return StatusCode(500, "Houve um erro no servidor");
            }
        }

        [HttpPost("registrar-venda")]
        public async Task<ActionResult> RegistrarVenda([FromBody]Venda venda)
        {
            if (venda == null) return BadRequest("Objeto inválido");

            var result = await _service.Adicionar(venda);

            if (!result) return BadRequest("Não foi possível cadastrar a venda!");
            
            return CustomResponse(result);

        }

        [HttpPut("atualizar-venda/{id:guid}/{status:int}")]
        public async Task<ActionResult> AtualizarVenda(
            [FromRoute] Guid id,
            [FromRoute] int status)
        {
            var result = await _service.Atualizar(id, status);

            if (!result) return BadRequest("Não foi possível atualizar o resultado!");

            return Ok(result);
        }
    }
}
