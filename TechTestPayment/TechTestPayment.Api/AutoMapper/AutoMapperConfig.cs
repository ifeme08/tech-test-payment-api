﻿using AutoMapper;
using TechTestPayment.Api.ViewModels;
using TechTestPayment.Domain.Entidades;

namespace TechTestPayment.Api.AutoMapper
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            // Domain to VM
            CreateMap<Item, ItemViewModel>().ReverseMap();
            CreateMap<Venda, VendaViewModel>()
                .ForMember(v => v.Data, vm => vm.MapFrom(s => s.Data))
                .ForMember(v => v.Status, vm => vm.MapFrom(s => s.Status))
                .ForMember(v => v.Itens, vm => vm.MapFrom(s => s.Itens))
                .ForMember(v => v.Vendedor, vm => vm.MapFrom(s => s.Vendedor));
            CreateMap<Vendedor, VendedorViewModel>().ReverseMap();

            // VM to Domain

            CreateMap<ItemViewModel, Item>()
                .ConstructUsing(i => new Item(i.Nome, i.ValorUnitario, i.Quantidade));
            CreateMap<VendaViewModel, Venda>()
                .ConstructUsing(v => new Venda());
            CreateMap<VendedorViewModel, Vendedor>()
                .ConstructUsing(v => new Vendedor(v.CPF, v.Nome, v.Email, v.Telefone));
        }
    }
}
