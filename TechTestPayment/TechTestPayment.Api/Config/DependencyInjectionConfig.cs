﻿using TechTestPayment.Application;
using TechTestPayment.Application.Validators;
using TechTestPayment.Data;
using TechTestPayment.Data.Repositorios;
using TechTestPayment.Domain.ObjetosDominio;

namespace TechTestPayment.Api.Config
{
    public static class DependencyInjectionConfig
    {

        public static IServiceCollection ResolveDependencies(this IServiceCollection services)
        {
            services.AddScoped<ApplicationDbContext>();
            services.AddScoped<IVendaRepository, VendasRepository>();
            services.AddScoped<IVendaService, VendaService>();
            services.AddScoped<INotificador, Notificador>();
            services.AddScoped<ItemValidator>();
            services.AddScoped<VendedorValidator>();

            return services;
        }
    }
}
