﻿using System.ComponentModel.DataAnnotations;

namespace TechTestPayment.Api.ViewModels
{
    public class VendaViewModel
    {
        [Key]
        public Guid Id { get; set; }
        public DateTime Data { get; set; }
        public int Status { get; set; }
        public Guid VendedorId { get; set; }
        public VendedorViewModel Vendedor { get; set; }
        public List<ItemViewModel> Itens { get; set; }

    }
}
