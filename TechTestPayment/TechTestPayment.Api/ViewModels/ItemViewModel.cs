﻿using System.ComponentModel.DataAnnotations;

namespace TechTestPayment.Api.ViewModels
{
    public class ItemViewModel
    {
        [Key]
        public Guid Id { get; set; }
        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        [StringLength(50, ErrorMessage = "O campo {0} precisa ter entre {2} e {1} caracteres", MinimumLength = 5)]
        public string Nome { get; set; }
        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public decimal Preco { get; set; }
        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public int Quantidade { get; set; }
        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public decimal ValorUnitario { get; set; }
        [Required(ErrorMessage = "O campo {0} é obrigatório")]
        public Guid VendaId { get; set; }
        public VendaViewModel Venda { get; set; }
    }
}
