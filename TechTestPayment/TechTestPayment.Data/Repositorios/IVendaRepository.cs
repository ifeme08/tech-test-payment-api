﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTestPayment.Domain.Entidades;

namespace TechTestPayment.Data.Repositorios
{
    public interface IVendaRepository : IRepository<Venda>
    {
        Task<Venda> BuscarVenda(Guid id);
        Task<bool> Adicionar(Venda venda);
        Task<bool> Atualizar(Guid id, int status);
    }
}
