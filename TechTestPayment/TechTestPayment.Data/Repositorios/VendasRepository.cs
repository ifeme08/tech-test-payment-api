﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTestPayment.Domain.Entidades;

namespace TechTestPayment.Data.Repositorios
{
    public class VendasRepository : IVendaRepository
    {
        private readonly ApplicationDbContext _context;
        public IUnitOfWork UnitOfWork => _context;

        public VendasRepository(ApplicationDbContext context)
        {
            _context = context;
        }


        public void Dispose()
        {
            _context.Dispose();
        }

        public async Task<Venda> BuscarVenda(Guid id)
        {
            var venda = await _context.Vendas
                                        .Include(v => v.Itens)
                                        .Include(v => v.Vendedor)
                                        .FirstOrDefaultAsync(x => x.Id == id);
            if (venda == null)
                return null;

            return venda;
        }

        public async Task<bool> Adicionar(Venda venda)
        {
            if (venda == null)
                return false;

            await _context.Vendas.AddAsync(venda);

            return await UnitOfWork.Commit();
        }

        public async Task<bool> Atualizar(Guid id, int status)
        {
            var _venda = await _context.Vendas.FirstOrDefaultAsync(v => v.Id == id);

            if (_venda == null)
                return false;

            _venda.AlterarStatus(status);

            _context.Vendas.Update(_venda);

            return await UnitOfWork.Commit();
        }
    }
}
