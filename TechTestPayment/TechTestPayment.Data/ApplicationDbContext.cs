﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTestPayment.Domain.Entidades;

namespace TechTestPayment.Data
{
    public class ApplicationDbContext : DbContext, IUnitOfWork
    {

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Item> Itens { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }

        public async Task<bool> Commit()
        {
            return await base.SaveChangesAsync() > 0;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var property in modelBuilder.Model.GetEntityTypes().SelectMany(
                e => e.GetProperties().Where(p => p.ClrType == typeof(string))))
                property.SetColumnType("varchar(100)");

            modelBuilder.Entity<Item>().HasData(
                new Item("Notebook Alienware", 12000m, 20),
                new Item("Monitor AOC", 1400m, 20),
                new Item("Monitor Samsung", 1500m, 20),
                new Item("Teclado RedDragon Karura", 160.40m, 20),
                new Item("Mouse Gamer Adamantium", 110.25m, 20),
                new Item("Motherboard x99 Huananzhi", 650.60m, 20),
                new Item("Memoria RAM NETAC 8gb 3200mhz", 163m, 20)
                );

            modelBuilder.Entity<Vendedor>().HasData(
                new Vendedor(cPF: "02678843654", nome:"Icaro Felox de Menezes", email:"ifeme08@gmail.com", telefone: "71992368543"),
                new Vendedor(cPF: "02678843654", nome: "Durin IV", email: "khazad_dum@gmail.com", telefone: "71995468543"),
                new Vendedor(cPF: "02678843654", nome: "Galadriel FinarfinDaughter", email: "supernoldor@gmail.com", telefone: "73985368543"),
                new Vendedor(cPF: "02678843654", nome: "Fëanor Noldor", email: "superfirespirit@gmail.com", telefone: "71981257432")
                );

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ApplicationDbContext).Assembly);

            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys())) relationship.DeleteBehavior = DeleteBehavior.ClientSetNull;
            base.OnModelCreating(modelBuilder);
        }
    }
}
