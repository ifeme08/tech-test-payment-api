﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTestPayment.Domain.Entidades;

namespace TechTestPayment.Data.Mappings
{
    public class VendaMapping : IEntityTypeConfiguration<Venda>
    {
        public void Configure(EntityTypeBuilder<Venda> builder)
        {
            builder.ToTable("Vendas");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Data)
                .IsRequired()
                .HasDefaultValue(DateTime.Now)
                .HasColumnType("DATETIME");

            builder.Property(x => x.Status)
                .IsRequired()
                .HasConversion(x => x.ToString(), x => (Status)Enum.Parse(typeof(Status), x));

            builder.HasOne(x => x.Vendedor)
                .WithMany(v => v.Vendas)
                .HasForeignKey(x => x.VendedorId);

            builder.HasMany(x => x.Itens)
                .WithOne(v => v.Venda)
                .HasForeignKey(x => x.VendaId);
        }
    }
}
