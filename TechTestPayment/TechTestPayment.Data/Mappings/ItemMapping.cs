﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTestPayment.Domain.Entidades;

namespace TechTestPayment.Data.Mappings
{
    public class ItemMapping : IEntityTypeConfiguration<Item>
    {
        public void Configure(EntityTypeBuilder<Item> builder)
        {
            builder.ToTable("Itens");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Preco)
                .IsRequired()
                .HasColumnType("DECIMAL(10,2)");

            builder.Property(x => x.Quantidade)
                .IsRequired()
                .HasColumnType("INT");

            builder.Property(x => x.Nome)
                .IsRequired()
                .HasColumnType("VARCHAR(50)");

            builder.HasIndex(x => x.Nome)
                .IsUnique();

            builder.HasOne(x => x.Venda)
                .WithMany(x => x.Itens)
                .HasForeignKey(x => x.VendaId);
        }
    }
}
