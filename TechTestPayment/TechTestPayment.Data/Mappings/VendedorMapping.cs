﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTestPayment.Domain.Entidades;

namespace TechTestPayment.Data.Mappings
{
    public class VendedorMapping : IEntityTypeConfiguration<Vendedor>
    {
        public void Configure(EntityTypeBuilder<Vendedor> builder)
        {
            builder.ToTable("Vendedores");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.CPF)
                .IsRequired()
                .HasColumnType("VARCHAR(11)")
                .HasMaxLength(11);

            builder.Property(x => x.Nome)
                .IsRequired()
                .HasColumnType("VARCHAR(50)")
                .HasMaxLength(50);

            builder.Property(x => x.Telefone)
                .IsRequired()
                .HasColumnType("VARCHAR(11)")
                .HasMaxLength(11);

            builder.Property(x => x.Email)
                .IsRequired()
                .HasColumnType("VARCHAR(30)")
                .HasMaxLength(30);

            builder.HasIndex(x => x.CPF)
                .IsUnique();

            builder.HasMany(x => x.Vendas)
                .WithOne(v => v.Vendedor)
                .HasForeignKey(x => x.VendedorId);
        }
    }
}
